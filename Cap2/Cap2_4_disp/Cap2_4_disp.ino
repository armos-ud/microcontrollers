#include <LiquidCrystal.h>

LiquidCrystal lcd(18, 5, 19, 21, 22, 23); //Pines (rs, en, d4, d5, d6, d7)

void setup() 
{
  lcd.begin(16, 2);
  lcd.setCursor(10, 0); //Mensaje centrado
  lcd.print("ESP32");
  lcd.setCursor(5, 1);
  lcd.print("Desplazamiento de texto");
}

void loop() 
{
  lcd.scrollDisplayRight(); //Desplaza a la derecha
  delay(250);
}
