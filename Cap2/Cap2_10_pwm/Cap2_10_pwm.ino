const int Canal = 0; // Canal PWM
const int Frec = 10000; // Frecuencia de conmutacion
const int Resolucion = 8; // Resolucion ciclo util
const int SalidaPWM = 23; // GPIO con PWM
 
void setup(){
  ledcSetup(Canal, Frec, Resolucion); // Configuracion PWM LED
  ledcAttachPin(SalidaPWM, Canal); // Asignacion GPIO: (GPIO, canal)
}
 
void loop()
{
  for(int CicloUtil = 0; CicloUtil <= 255; CicloUtil++) // Aumento ancho de pulso
  {   
    ledcWrite(Canal, CicloUtil);
    delay(5); // Retardo entre actualizaciones
  }

  for(int CicloUtil = 255; CicloUtil >= 0; CicloUtil--) // Reduccion ancho de pulso
  {
    ledcWrite(Canal, CicloUtil);   
    delay(5); // Retardo entre actualizaciones
  }
}
