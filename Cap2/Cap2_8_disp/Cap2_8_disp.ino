#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

Adafruit_PCD8544 display = Adafruit_PCD8544(15, 2, 4, 5, 19); // (CLK,DIN,DC,CE,RST)

void setup()   {
  Serial.begin(9600);

  display.begin();
  display.setContrast(60);
  display.clearDisplay();

  display.drawRect(0, 0, 60, 40, BLACK); // Rectangulo
  display.display();
  delay(2000);
  display.clearDisplay();

  display.fillRect(0, 0, 60, 40, BLACK); // Rectangulo lleno
  display.display();
  delay(2000);
  display.clearDisplay();

  display.drawRoundRect(0, 0, 60, 40, 8, BLACK); // Rectangulo esquinas redondeadas
  display.display();
  delay(2000);
  display.clearDisplay();

  display.fillRoundRect(0, 0, 60, 40, 8, BLACK); // Rectangulo lleno esquinas redondeadas
  display.display();
  delay(2000);
  display.clearDisplay();

  display.drawCircle(20, 20, 20, BLACK); // Circulo
  display.display();
  delay(2000);
  display.clearDisplay();

  display.fillCircle(20, 20, 20, BLACK); // Circulo lleno
  display.display();
  delay(2000);
  display.clearDisplay();

  display.drawTriangle(20, 0, 0, 40, 40, 40, BLACK); // Triangulo
  display.display();
  delay(2000);
  display.clearDisplay();

  display.fillTriangle(20, 0, 0, 40, 40, 40, BLACK); // Triangulo lleno
  display.display();
  delay(2000);
  display.clearDisplay();
}

void loop() {}
