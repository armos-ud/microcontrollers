#include <Keypad.h>
 
const byte rows = 4; //Numero de filas
const byte cols = 4; //Numero de columnas
 
char keys[rows][cols] = {
   { '1','2','3', 'A' },
   { '4','5','6', 'B' },
   { '7','8','9', 'C' },
   { '*','0','#', 'D' }
};
 
byte rowPins[rows] = { 13, 12, 14, 27 }; //Filas del teclado
byte colPins[cols] = { 26, 25, 33, 32 }; //Columnas del teclado
 
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, rows, cols);
 
void setup() 
{
   Serial.begin(115200);
}
 
void loop() 
{
   char key = keypad.getKey();
 
   if (key != NO_KEY) {
      Serial.println(key);
   }
}
