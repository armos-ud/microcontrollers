#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

// Declarar el objeto LCD con: (CLK,DIN,DC,CE,RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(15, 2, 4, 5, 19);

int rotartexto = 1;

void setup()   {
  display.begin(); // Inicializa la pantalla
  display.setContrast(60); // Ajuste del contraste
  display.clearDisplay(); // Limpia el buffer

  // Texto en pantalla
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.println("Hello world!");
  display.display();
  delay(2000);
  display.clearDisplay();


  // Texto invertido
  display.setTextColor(WHITE, BLACK); // 'inverted' text
  display.setCursor(0,10);
  display.println("Hello world!");
  display.display();
  delay(2000);
  display.clearDisplay();

  // Escalado de la fuente
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.setTextSize(2);
  display.println("Hello!");
  display.display();
  delay(2000);
  display.clearDisplay();

  // Numeros
  display.setTextSize(1);
  display.setCursor(0,0);
  display.println(123456789);
  display.display();
  delay(2000);
  display.clearDisplay();

  // Especificando la base numerica para numeros
  display.setCursor(0,0);
  display.print("0x"); display.print(0xFF, HEX); 
  display.print("(HEX) = ");
  display.print(0xFF, DEC);
  display.println("(DEC)"); 
  display.display();
  delay(2000);
  display.clearDisplay();

  // Caracteres en ASCII
  display.setCursor(0,0);
  display.setTextSize(5);
  display.write(5); // 0 a 255
  display.display();
  delay(2000);
  display.clearDisplay();

  // Rotacion de texto
  while(1)
  {
  display.clearDisplay();
  display.setRotation(rotartexto);  // Rotar 90 grados antihorario
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.println("Rotando");
  display.display();
  delay(1000);
  display.clearDisplay();
  rotartexto++; // Varia angulo de rotacion
  }
}

void loop() {}
