int sensor1 = 36;   // Pin entrada ADC señal de salida
int muestreo = 2;   // Led para visualizar periodo de muestreo
float Vout1;        // Variable de salida a controlar (velocidad, temperatura, etc)
float r1 = 1.0;     // Valor de la entrada de referencia, manipulada por codigo
float aux;          // Variable auxiliar en la lectura del ADC, manipulada por codigo
float Ksensor = 1.0;  // Ganancia del sensor, manipulada por codigo

byte Ts = 1;     // Periodo de muestreo

hw_timer_t *My_timer = NULL; // Variable de tipo hw_timer_t para configurar timer

float kp = 700;  // Parametros del PID
float ti = 7;
float td = 0;

float q0,q1,q2;  // Parámetros señal de control
volatile float u = 0.0, u_1 = 0.0;    // Señal de control, desde RAM por rutina interrupcion
volatile float e = 0.0, e_1 = 0.0, e_2 = 0.0; // Variables para error, desde RAM

const int Canal = 0;      // Canal PWM
const int Frec = 10000;   // Frecuencia del PWM
const int Resolucion = 8; // Resolucion ciclo util, varia de 0 a 255!!!
const int SalidaPWM = 23; // GPIO con PWM

// Rutina de servicio para la interrupcion del timer
void IRAM_ATTR onTimer()
{
  digitalWrite(muestreo, !digitalRead(muestreo)); // Led Toggle
  PID();
}

void setup() {
  Serial.begin(115200);

  ledcSetup(Canal, Frec, Resolucion); // Configuracion PWM
  ledcAttachPin(SalidaPWM, Canal);    // Asignacion PWM a GPIO
  
  pinMode(muestreo,OUTPUT);  // Led con periodo de muestreo
  digitalWrite(muestreo,LOW);

  // Timer: Hz = Clock_speed[MHz]/prescaler, (ESP32 a 80 MHz), configurado a 1 seg, cuanta a 1000000
  My_timer = timerBegin(0, 80, true);  // Inicializa timer: (timer_a_usar_0_a_3, prescaler, up_(true)_o_down (false))
  timerAttachInterrupt(My_timer, &onTimer, true); // Conecta interrupcion a la rutina de servicio
  timerAlarmWrite(My_timer, 1000000, true);  // Valor del contador que genera interrupcion (1 s), true lo hace periodico
  timerAlarmEnable(My_timer);  // Habilitamos la interrupcion por timer

  // Calculo parametros PID digital
  q0 = kp*(1+Ts/(2.0*ti)+td/Ts);
  q1 = -kp*(1-Ts/(2.0*ti)+(2.0*td)/Ts);
  q2 = (kp*td)/Ts;
}

void loop() {
  int i;

  aux = 0;  // Filtro  de promedio movil en la lectura del ADC
  for(i=0;i<10;i++){
    aux = aux + (float(analogRead(sensor1))*3.3/4095.0)*Ksensor; // Se debe escalar de acuerdo al sensor
  }
  Vout1 = aux/10.0;  // Valor promediado de 10 lecturas

  Serial.println("Variable salida 1, Setpoint 1");
  Serial.print(Vout1);
  Serial.print(","); 
  Serial.println(r1); 

  delay(1000);
}

void PID(void)
{
  e = (r1 - Vout1);   // Calculo de error actual
  u = u_1 + q0*e + q1*e_1 + q2*e_2; // Ley del control PID discreto

  if (u >= 100.0)  // Saturo la accion de control en un tope maximo y minimo
    u = 100.0;

  if (u <= 0.0 || r1==0)
    u = 0.0;
     
  e_2=e_1; // Avanzo un instante de muertreo
  e_1=e;
  u_1=u;
     
  // Transformo la accion de control en PWM
  ledcWrite(Canal, map(u, 0,100, 0,255)); // Salida de control PWM     
}
