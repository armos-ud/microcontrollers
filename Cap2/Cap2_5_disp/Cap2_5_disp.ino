#include <LiquidCrystal.h>

LiquidCrystal lcd(18, 5, 19, 21, 22, 23); //(rs, en, d4, d5, d6, d7)

byte cuerpo[8] = {
B01110,
B01110,
B00100,
B11111,
B00100,
B00100,
B01010,
B10001,
};
byte flecha1[8] = {
B01000,
B00100,
B00110,
B11111,
B11111,
B00110,
B00100,
B01000,
};
byte flecha2[8] = {
B00010,
B00100,
B01100,
B11111,
B11111,
B01100,
B00100,
B00010,
};

void setup () {
  lcd.createChar (0,cuerpo); //Nuevos simbolos, maximo 8
  lcd.createChar (1,flecha1);
  lcd.createChar (2,flecha2);

  lcd.begin (16, 2); 
  
  lcd.print("Nuevo Caracter:"); // Escribimos el texto en el LCD
  lcd.setCursor(2, 1); //(columna[0 a 15],fila[0 a 2])
  lcd.print("---");
  lcd.write (byte (1));
  lcd.write (byte (1));
  lcd.write (byte (0));
  lcd.write (byte (0));
  lcd.write (byte (2));
  lcd.write (byte (2));
  lcd.print("---");  
}

void loop () {}
