#include "WiFi.h"

void setup(){
  Serial.begin(115200); // Monitor serial
  
  // Configura WiFi a station mode y desconecta de AP si estaba conectado
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  
  Serial.println("Configuracion WiFi terminada");
}
 
void loop()
{
  int start = millis(); // Captura instante inicial
  Serial.println(" ");
  Serial.println("Inicio de escaneo...");
  // WiFi.scanNetworks retorna el numero de redes encontradas
  int n = WiFi.scanNetworks();
  int finish = millis();
  Serial.print("Escaneo terminado, duracion = ");
  Serial.print(finish - start);
  Serial.println(" ms");
  if (n == 0) {
    Serial.println("No se encontraron redes");
  } else {
    Serial.print(n);
    Serial.println(" redes encontradas");
    for (int i = 0; i < n; ++i) { // Imprime SSID y RSSI de cada red encontrada
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*");
      delay(10);
    }
  }
  Serial.print(""); // Espera un poco antes de escanear nuevamente
  delay(10000);
}
