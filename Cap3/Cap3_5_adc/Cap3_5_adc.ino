#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

Adafruit_PCD8544 display = Adafruit_PCD8544(15, 2, 4, 5, 19); // (CLK,DIN,DC,CE,RTS)

int entradaValor;
int pinADC = 36; // Puerto seleccionado para el ADC
 
void setup(){
  Serial.begin(115200); // Monitor serial
  
  display.begin(); // Inicializa la pantalla
  display.setContrast(60); // Ajuste del contraste
  display.clearDisplay(); // Limpia el buffer
}
 
void loop()
{
  display.clearDisplay(); // Limpia el buffer
  display.setTextSize(1); // Texto inicial
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.println("Lectura de ADC");
  display.display();

  display.setTextSize(1); // Visualizacion texto info
  display.setTextColor(BLACK);
  display.setCursor(0,12); // columnas 84, filas 48
  display.println("Valor entrada:");
  display.display();
  
  entradaValor = analogRead(pinADC); // GPI 36,es solo de entrada!!!

  Serial.print("Valor entrada: "); // Monitor serial
  Serial.println(entradaValor);

  display.setTextSize(2); // Visualizacion en 5110
  display.setCursor(12,22); // columnas 84, filas 48
  display.println(entradaValor);
  display.display();

  delay(1000);
}
