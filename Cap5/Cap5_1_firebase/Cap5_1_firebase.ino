#include <Arduino.h>
#include <WiFi.h>
#include <Firebase_ESP_Client.h>
#include "addons/TokenHelper.h"
#include "addons/RTDBHelper.h"

// Introduzca credenciales de su red
#define WIFI_SSID "RedmiFM"
#define WIFI_PASSWORD "11111111"

// Coloque clave de API web de Firebase
#define API_KEY "AIzaSyDUowkAwxXXxxxXXxx4q5mOfyBuSSo4sY"

// Coloque correo electronico y contraseña
#define USER_EMAIL "usuario@correo.co"
#define USER_PASSWORD "password1234"

// Coloque dirección URL de la base de datos
#define DATABASE_URL "https://esp32-firebase.firebaseio.com"

// Define los objetos de Firebase
FirebaseData stream;
FirebaseAuth auth;
FirebaseConfig config;

// Variable para guardar la ruta (path) de las bases de datos
String listenerPath = "tarjeta1/salidas/digitales/";

// Salidas a manejar por este ESP32 (otros ESP32 pueden manejar otros GPIO)
const int output1 = 19;
const int output2 = 21;
const int output3 = 22;
const int output4 = 23;

void setup(){
  Serial.begin(115200);
  initWiFi(); // Llama rutina que inicia conexion WiFi

  pinMode(output1, OUTPUT);
  pinMode(output2, OUTPUT);
  pinMode(output3, OUTPUT);
  pinMode(output4, OUTPUT);
  
  config.api_key = API_KEY; // Asigna la clave API

  auth.user.email = USER_EMAIL; // Credenciales de usuario
  auth.user.password = USER_PASSWORD;

  config.database_url = DATABASE_URL; // URL de la base de datos

  Firebase.reconnectWiFi(true);

  config.token_status_callback = tokenStatusCallback;
  config.max_token_generation_retry = 5;

  Firebase.begin(&config, &auth);

  if (!Firebase.RTDB.beginStream(&stream, listenerPath.c_str()))
    Serial.printf("Error de inicio de flujo, %s\n\n", stream.errorReason().c_str());

  Firebase.RTDB.setStreamCallback(&stream, streamCallback, streamTimeoutCallback);

  delay(2000);
}

void loop(){}

// Rutina que iInicializa WiFi
void initWiFi() {
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Conectando a la red WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
  Serial.println();
}

// Funcion que regresa los cambios en la base de datos
void streamCallback(FirebaseStream data){
  Serial.printf("Path del flujo: %s\npath del evento, %s\ntipo de dato, %s\ntipo de evento, %s\n\n",
                data.streamPath().c_str(),
                data.dataPath().c_str(),
                data.dataType().c_str(),
                data.eventType().c_str());
  printResult(data);
  Serial.println();

  String streamPath = String(data.dataPath());

  if (data.dataTypeEnum() == fb_esp_rtdb_data_type_integer){
    String gpio = streamPath.substring(1);
    int state = data.intData();
    Serial.print("GPIO: ");
    Serial.println(gpio);
    Serial.print("ESTADO: ");
    Serial.println(state);
    digitalWrite(gpio.toInt(), state);
  }

  if (data.dataTypeEnum() == fb_esp_rtdb_data_type_json){
    FirebaseJson json = data.to<FirebaseJson>();

    size_t count = json.iteratorBegin();
    Serial.println("\n---------");
    for (size_t i = 0; i < count; i++){
        FirebaseJson::IteratorValue value = json.valueAt(i);
        int gpio = value.key.toInt();
        int state = value.value.toInt();
        Serial.print("ESTADO: ");
        Serial.println(state);
        Serial.print("GPIO:");
        Serial.println(gpio);
        digitalWrite(gpio, state);
        Serial.printf("Nombre: %s, Valor: %s, Tipo: %s\n", value.key.c_str(), value.value.c_str(), value.type == FirebaseJson::JSON_OBJECT ? "object" : "array");
    }
    Serial.println();
    json.iteratorEnd();
  }
  
  Serial.printf("Tamaño del flujo recibido: %d (Max. %d)\n\n", data.payloadLength(), data.maxPayloadLength());
}

void streamTimeoutCallback(bool timeout){
  if (timeout)
    Serial.println("stream timeout, reanudando...\n");
  if (!stream.httpConnected())
    Serial.printf("codigo de error: %d, razon: %s\n\n", stream.httpCode(), stream.errorReason().c_str());
}
