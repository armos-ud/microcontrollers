#include <LiquidCrystal.h>

//Crear el objeto LCD con los pines (rs, en, d4, d5, d6, d7) del ESP32
LiquidCrystal lcd(18, 5, 19, 21, 22, 23);

void setup()
{
  lcd.begin(16, 2); //Inicializa LCD con numero de filas y columnas
  lcd.print("Pantalla LCD16x2"); //Mensaje con 16 caracteres
}

void loop()
{
   // Mueve cursor a primera posición (columna 0) de la segunda fila (fila 1)
  lcd.setCursor(0, 1);
   // Escribimos el número de segundos trascurridos
  lcd.print(millis()/1000);
  lcd.print(" Segundos");
  delay(100);
}
